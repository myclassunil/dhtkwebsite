# DHTK Gutenberg Extension API
::: dhtk.extensions.gutenberg.api
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2

::: dhtk.extensions.gutenberg.tools
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2