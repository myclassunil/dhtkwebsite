# DHTK Core API
::: dhtk.core.client
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2

::: dhtk.core.loader
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2

::: dhtk.core.settings
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2

::: dhtk.core.client
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2

::: dhtk.methods.lod.link_dbpedia
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2

::: dhtk.methods.lod.link_viaf
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2

::: dhtk.methods.lod.link_wikidata
    rendering:
        show_root_toc_entry: true
        show_category_heading: true
        show_source: false
        heading_level: 2